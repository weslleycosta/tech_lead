# language: pt

Funcionalidade: Buscas no google

  Contexto: Estar na página de pesquisa do google
  Dado que eu visito "google"

  @valido
  Cenario: Busca por webjump com sucesso
  E Busco por "weslley" na barra de pesquisa
  Entao Vejo texto "webjump.com.br"  

  @invalido
  Cenario: Busca sem sucesso
  E Busco por "whewshehew" na barra de pesquisa
  Entao Vejo texto " - não encontrou nenhum documento correspondente."

  @time
  Cenario: Busca de time de futebol
  E Busco por "corinthians" na barra de pesquisa
  E Seleciono time buscado
  Entao Vejo texto "Sport Club Corinthians Paulista"