require 'base64'
require 'selenium-webdriver'

Before do |scn|
  @google = Google.new
end

After do |scn|
  binding.pry if ENV['debug']
end

After do
  shot_file = page.save_screenshot("log/screenshot.png")
  shot_b64 = Base64.encode64(File.open(shot_file, "rb").read)
  embed(shot_b64, "image/png", "Screenshot") # cucumber anexa o screenshot no report
end