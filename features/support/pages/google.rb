class Google

  include RSpec::Matchers
  include Capybara::DSL

  def initialize
    @campo_busca         = EL['campo_busca']
    @selecionando_time   = EL['selecionando_time']
  end

  def busca(nome)
    find(@campo_busca).set(nome)
    find(@campo_busca).native.send_keys(:enter)
  end

  def clicando_time
    find(@selecionando_time).click
  end

end
