# visitando a pagina
Dado('que eu visito {string}') do |string|
  url = DATA[string]
  visit(url)
end

Entao('Busco por {string} na barra de pesquisa') do |texto|
  @google.busca(texto)
end

Entao('Vejo texto {string}') do |texto|
  assert_text(texto)
end

Entao('Seleciono time buscado') do
  @google.clicando_time
end